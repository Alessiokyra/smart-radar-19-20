#ifndef __SCANSIONE__
#define __SCANSIONE__

#include "Task.h"
#include "ServoTimer2.h"
#include "Utilities.h"

class Scansione: public Task {


  public:

    Scansione(int enginePin, int trigPin, int echoPin);
    void init(int period);
    void tick();
  private:
    int enginePin;
    int trigPin;
    int echoPin;
    ServoTimer2* engine;

};
#endif
