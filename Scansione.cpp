#include "Scansione.h"
#include "Utilities.h"
#include "ServoTimer2.h"



Scansione::Scansione(int enginePin, int echoPin, int trigPin) {
  this->enginePin = enginePin;
  this->echoPin = echoPin;
  this->trigPin = trigPin;
}

void Scansione::init(int period) {
  Task::init(period);
  engine = new ServoTimer2();
  engine->attach(this->enginePin);
  pinMode(8, OUTPUT);
  pinMode(7, INPUT);
}





void Scansione::tick() {

  if (servoInPosition) {
    // FAI SCANSIONE
    digitalWrite(8, LOW);
    delayMicroseconds(3);
    digitalWrite(8, HIGH);
    delayMicroseconds(5);
    digitalWrite(8, LOW);
    long tUS = pulseIn(7, HIGH, 6000);
    distance = tUS * 0.0171925;
    servoInPosition = false;
    Serial.println(distance);
  }

  if ((currentMode == 1 && pirFound) || currentMode == 3) {
    if (i == N)
      i = 0;
    deltaAngle = 180 / N;
    engine -> write(750 + (deltaAngle * coeff * i));
    i++;
    servoInPosition = true;
  }
  if (currentMode == 2) { //aggiungere quando invio nuovo angolo servo in position = false
    engine -> write(750 + (setAngle * coeff));
    servoInPosition = true;
  }
}
