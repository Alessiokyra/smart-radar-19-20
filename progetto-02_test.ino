#include "Scansione.h"
#include "Utilities.h"
int distance = 0;
int tempAngle = 800;
int setAngle ;
int currentAngle;
int deltaAngle;
bool servoInPosition = 0;
bool scanned;
bool alarm = 0; //mettere 0
bool tempAlarm = 0;
int currentMode = 2;
int tempoUtente;
bool pirFound;
int i = 0;
float coeff = (2250.0 - 750.0) / 180;

Scheduler scheduler;
ModeSetter mS;

//TO-DO test Blink.
Task* pirSleep;
Task* leds;
Task* scan;
Task* buttonListener;


void setup() {
  Serial.begin(115200);
  Serial.println("beginning");
  scheduler.init(5);
  scan = new Scansione(ENGINE_PIN, ECHO_PIN, TRIG_PIN);
  buttonListener = new ButtonListener(TM1, TM2, TM3);
  leds = new Blink(LED_A, LED_D);
  pirSleep = new PirSleep(PIR_PIN);

  //inizializzo tutti i task
  scan->init(1000);
  pirSleep->init(40);
  leds->init(50);
  buttonListener-> init(50);





  scheduler.addTask(scan);
  scheduler.addTask(buttonListener);
  scheduler.addTask(leds);
  scheduler.addTask(pirSleep);
  //Aggiungere calibrazione Pir



  scan->setActive(true);
  buttonListener->setActive(false);
}


void loop() {
  scheduler.schedule();
}

void serialEvent() { //ogni volta che finisce il loop viene chiamata questa funzione, per vedere se arrivano nuovi dati.

  char serial;
  while (Serial.available()) {
    serial = (char)Serial.read(); //Leggo il primo valore della seriale per vedere che informazione arriva

    if (serial != '\n') {
      switch (serial) {

        case 't':
          if (Serial.peek() == 'p') {
            tempoUtente = map(analogRead(POT_PIN), 0, 1024, MIN_TIME, MAX_TIME);
            Serial.read();
          }
          else {
            int tTemp = Serial.parseInt();
            if (tTemp <= MAX_TIME && tTemp >= MIN_TIME) // 2- 10
              tempoUtente = tTemp;
            else
              Serial.println("Out Of Bound Time, must be included between: " + (String)MIN_TIME + " " + "and " + (String)MAX_TIME + " !");

          }
          Serial.print("User Time set to: ");
          Serial.print(tempoUtente);
          Serial.println(" Seconds !");
          //taskServo->init(180/(tempoUtente * 1000)); //converto in millisecondi il valore ottenuto e lo divido per 180 per vedere quanto deve rimanere in una singola posizione
          break;


        case 'd':
        setAngle = Serial.parseInt();
        
          Serial.println("Angle set to: " + (String)setAngle);
          break;

        case 's':                                                  //SINGLE

          Serial.println("Mode set to: SINGLE (1)");
          mS.setMode(1);
          break;

        case 'm':                                                    //MANUAL
          Serial.println("Mode set to: MANUAL (2)");
          mS.setMode(2);
          break;

        case 'a':                                                     //AUTO
          mS.setMode(3);
          Serial.println("Mode set to: AUTO (3)");
          break;

        default:
          Serial.print("Command Not Found! : ");
          Serial.println(serial);
          break;
      }
    }
  }
}
