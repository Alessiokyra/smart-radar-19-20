#ifndef PIRSLEEP
#define PIRSLEEP

#include "Task.h"

class PirSleep: public Task {
 
    int pirPin;
  public:
    PirSleep(int pirPin);
    void init(int period);
    void tick();
};
#endif
