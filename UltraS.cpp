#include "UltraS.h"
#include "Utilities.h"
#include "NewPing.h"


UltraS::UltraS(int trigPin, int echoPin) {
  this->sonar = new NewPing(trigPin, echoPin);
}

void UltraS::init(int period) {
  Task::init(period);
}

void UltraS::tick() {
  Serial.println(distance);
  if (servoInPosition) {
    distance = 30;   //sonar->ping();
    //servoInPosition = 0;
    //scanned = true;
  }
  if (distance <= MIN_DIST) { // && distance <= MAX_DIST && currentMode == 3) {
    alarm = 1;
    tempAlarm = 1;
  }
}
