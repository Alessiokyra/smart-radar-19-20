#include "EngineServo.h"
#include "Utilities.h"
#include "ServoTimer2.h"


EngineServo::EngineServo(int enginePin) {
  this->engine = new ServoTimer2();
  this->engine->attach(enginePin);
  delay(500);
}

void EngineServo::init(int period) {
  Task::init(period);
}


void EngineServo::tick() {

 currentAngle = engine->read();
  if (currentAngle == 0 || currentAngle == 180) {
    if (tempAlarm == 1) {
      tempAlarm = 0;
    }
    else {
      alarm = 0;
    }
  }

  if (setAngle != currentAngle) {
    servoInPosition = false;
    scanned = false;
  }


  if (setAngle - currentAngle > 0)
    currentAngle--;

  if (setAngle - currentAngle < 0)
    currentAngle++;

  if (setAngle == currentAngle && scanned == 0)
    servoInPosition = true;

  engine->write(currentAngle);

}
