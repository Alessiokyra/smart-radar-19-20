#ifndef __ULTRAS__
#define __ULTRAS__

#include "Task.h"
#include "NewPing.h"
#include "Utilities.h"

class UltraS: public Task {
    NewPing* sonar;

  public:
    UltraS(int trigPin, int echoPin);
    void init(int period);
    void tick();
};

#endif
