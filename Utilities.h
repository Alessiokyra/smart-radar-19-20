#ifndef __UTILITIES__
#define __UTILITIES__


#include "Scheduler.h"
#include "PirSleep.h"
#include "Task.h"
#include "BlinkTask.h"
#include <avr/sleep.h>
#include <avr/power.h>
#include "TimerOne.h"
#include "ButtonListener.h"
#include "ModeSetter.h"

//VARIABILI
extern int distance;
extern int setAngle;
extern int currentAngle;
extern bool servoInPosition;
extern int currentMode;
extern int tempoUtente;
extern bool pirFound;
extern bool scanned;
extern bool alarm;
extern bool tempAlarm;
extern Scheduler scheduler;
extern ModeSetter mS;
extern Task* pirSleep;
extern Task* scan;
extern Task* leds;
extern Task* buttonListener;
extern int i;
extern int deltaAngle;
extern float coeff;



#define MIN_DIST 10
#define MAX_DIST 50

#define MAX_TIME 10
#define MIN_TIME 2
#define N 16

//circuito
#define PIR_PIN 9
#define POT_PIN  A5
#define TRIG_PIN 8 
#define ECHO_PIN 7
#define LED_A 13
#define LED_D 12
#define ENGINE_PIN 6
#define TM1 48
#define TM2 50
#define TM3 52

#endif
