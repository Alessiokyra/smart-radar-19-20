#include "ModeSetter.h"
#include "Utilities.h"

void ModeSetter::setMode(int mode) {
  if (mode != currentMode) {
    digitalWrite(LED_A, LOW);
    digitalWrite(LED_D, LOW);
    switch (mode) {

      case 1:                         //Single
        
        pirSleep->setActive(true);
        leds->setActive(true);
        currentMode = 1;
        break;

      case 2:
        pirSleep->setActive(false);
        leds->setActive(false);
        currentMode = 2;
        break;

      case 3:
        pirSleep->setActive(false);
        leds->setActive(true);
        currentMode = 3;
        break;
    }
    Serial.println("Mode set to: " + (String)currentMode);
  }
}
