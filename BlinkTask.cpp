#include "BlinkTask.h"
#include "Utilities.h"



Blink::Blink(int pinA, int pinD) {
  this->pinA = pinA;
  this->pinD = pinD;
}

void Blink::init(int period) {
  Task::init(period);
  ledA = new Led(pinA);
  ledD = new Led(pinD);
  state = OFF;
}

void Blink::tick() {

  switch (currentMode) {
    case 1: //SINGLE
      if (alarm && currentMode == 1) {
        switch (state) {
          case OFF:
            ledA->switchOn();
            state = ON;
            break;
          case ON:
            ledA->switchOff();
            state = OFF;
            break;
        }
      } else {
        ledA->switchOff();
        state = OFF;
      }

      break;

    case 3:

      if (distance >= MIN_DIST && distance <= MAX_DIST && distance != 0) {
        switch (state) {
          case OFF:
            ledD->switchOn();
            state = ON;
            break;
          case ON:
            ledD->switchOff();
            state = OFF;
            break;
        }
      }else {
        ledD->switchOff();
        state = OFF;
      }
      break;
  }
  /*

  */
}
