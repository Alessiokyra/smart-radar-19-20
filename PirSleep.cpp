#include "PirSleep.h"
#include "Utilities.h"

PirSleep::PirSleep(int pirPin) { 
  this->pirPin = pirPin;
  pinMode(pirPin, INPUT);
}

void PirSleep::init(int period) {
  Task::init(period);
}


void PirSleep::tick() {
  //Simulating sleep mode with a delay
  delay(30);
  
  /* set_sleep_mode(SLEEP_MODE_IDLE);
  sleep_enable();
  power_adc_disable();
  power_spi_disable();
  power_timer0_disable();
  power_timer2_disable();
  power_twi_disable();  
  sleep_mode();  
  sleep_disable();
  power_all_enable();*/
  pirFound = 1; //digitalRead(pirPin);
}
