#ifndef __SERVO__
#define __SERVO__

#include "Task.h"
#include "ServoTimer2.h"

class EngineServo: public Task {
    ServoTimer2* engine;

  public:
    EngineServo(int enginePin);
    void init(int period);
    void tick();
};
#endif
